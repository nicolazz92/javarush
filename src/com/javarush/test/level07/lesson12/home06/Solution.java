package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human). Создай объекты и заполни их так,
чтобы получилось: Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human, то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution {
    public static void main(String[] args) {
        Human grandMa1 = new Human("Аня", false, 76, null, null);
        Human grandFa1 = new Human("Петя", true, 78, null, null);
        Human grandMa2 = new Human("Катя", false, 70, null, null);
        Human grandFa2 = new Human("Лёха", true, 73, null, null);

        Human father = new Human("Pedro", true, 35, grandFa1, grandMa1);
        Human mother = new Human("Lucia", false, 36, grandFa2, grandMa2);

        Human child1 = new Human("child1", false, 11, father, mother);
        Human child2 = new Human("child2", false, 10, father, mother);
        Human child3 = new Human("child3", true, 9, father, mother);

        System.out.println(grandMa1);
        System.out.println(grandFa1);
        System.out.println(grandMa2);
        System.out.println(grandFa2);

        System.out.println(father);
        System.out.println(mother);

        System.out.println(child1);
        System.out.println(child2);
        System.out.println(child3);
    }

    public static class Human {
        //напишите тут ваш код
        String name;
        boolean sex;
        int age;
        Human father;
        Human mother;

        public Human(String name, boolean sex, int age, Human father, Human mother) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
