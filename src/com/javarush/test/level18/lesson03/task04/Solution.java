package com.javarush.test.level18.lesson03.task04;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* Самые редкие байты
Ввести с консоли имя файла
Найти байт или байты с минимальным количеством повторов
Вывести их на экран через пробел
Закрыть поток ввода-вывода
*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputStream = new FileInputStream(reader.readLine());
        reader.close();

        HashMap<Integer, Integer> map = new HashMap<>();
        while (inputStream.available() > 0) {
            int b = inputStream.read();
            if (map.get(b) == null) {
                map.put(b, 1);
            } else {
                map.put(b, map.get(b) + 1);
            }
        }
        inputStream.close();

        Iterator<Integer> iterator = map.values().iterator();
        int minRepead = Integer.MAX_VALUE;
        while (iterator.hasNext()) {
            int n = iterator.next();
            if (n < minRepead) {
                minRepead = n;
            }
        }

        Set<Map.Entry<Integer, Integer>> set = map.entrySet();

        for (Map.Entry<Integer, Integer> m : set) {
            if (m.getValue() == minRepead){
                System.out.print(m.getKey() + " ");
            }
        }
    }
}
