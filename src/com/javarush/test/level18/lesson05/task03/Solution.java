package com.javarush.test.level18.lesson05.task03;

/* Разделение файла
Считать с консоли три имени файла: файл1, файл2, файл3.
Разделить файл1 по следующему критерию:
Первую половину байт записать в файл2, вторую половину байт записать в файл3.
Если в файл1 количество байт нечетное, то файл2 должен содержать бОльшую часть.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        FileInputStream inputFile1 = new FileInputStream(reader.readLine());
        FileOutputStream outputFile2 = new FileOutputStream(reader.readLine());
        FileOutputStream outputFile3 = new FileOutputStream(reader.readLine());
        reader.close();

        if (inputFile1.available() > 0) {
            byte[] buffer = new byte[inputFile1.available()];
            int len = inputFile1.read(buffer);
            if (len % 2 == 0) {
                outputFile2.write(buffer, 0, (len / 2));
                outputFile3.write(buffer, len / 2, len / 2);
            } else {
                outputFile2.write(buffer, 0, (len) / 2 + 1);
                outputFile3.write(buffer, (len) / 2 + 1, len / 2);
            }
        }

        inputFile1.close();
        outputFile2.close();
        outputFile3.close();
    }
}
