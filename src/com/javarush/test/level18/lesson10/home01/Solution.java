package com.javarush.test.level18.lesson10.home01;

/* Английские буквы
В метод main первым параметром приходит имя файла.
Посчитать количество букв английского алфавита, которое есть в этом файле.
Вывести на экран число (количество букв)
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException {
        FileInputStream inputStream = new FileInputStream(args[0]);
        int count = 0;

        if (inputStream.available() > 0) {
            byte[] bytes = new byte[inputStream.available()];
            int lenght = inputStream.read(bytes);

            for (byte b : bytes) {
                if ((b >= 65 && b <= 90) || (b >= 97 && b <= 122)){
                    count++;
                }
            }
        }
        inputStream.close();
        System.out.println(count);
    }
}
