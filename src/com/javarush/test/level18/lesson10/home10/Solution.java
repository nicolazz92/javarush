package com.javarush.test.level18.lesson10.home10;

/* Собираем файл
Собираем файл из кусочков
Считывать с консоли имена файлов
Каждый файл имеет имя: [someName].partN. Например, Lion.avi.part1, Lion.avi.part2, ..., Lion.avi.part37.
Имена файлов подаются в произвольном порядке. Ввод заканчивается словом "end"
В папке, где находятся все прочтенные файлы, создать файл без приставки [.partN]. Например, Lion.avi
В него переписать все байты из файлов-частей используя буфер.
Файлы переписывать в строгой последовательности, сначала первую часть, потом вторую, ..., в конце - последнюю.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.*;

public class Solution {
    static Set<String> partsNames = new TreeSet<>();

    public static void main(String[] args) {
        readNames();
        String fileName = createFile(partsNames);
        writeData(partsNames, fileName);
    }

    public static void writeData(Set<String> set, String fileName) {
        try {
            FileOutputStream outputFile = new FileOutputStream(fileName, true);
            FileInputStream inputFile = null;

            for (String s : set) {
                inputFile = new FileInputStream(s);
                byte[] buffer = new byte[inputFile.available()];
                int len = inputFile.read(buffer);
                outputFile.write(buffer);
            }

            outputFile.close();
            if (inputFile != null) {
                inputFile.close();
            }
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static String createFile(Set<String> set) {
        StringBuilder string = new StringBuilder(set.iterator().next());
        string.delete(string.lastIndexOf("part") - 1, string.length());
        File file = new File(string.toString());
        try {
            if (file.createNewFile()) {
                System.out.println("file created");
            } else {
                file.delete();
                file.createNewFile();
                System.out.println("file recreated");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string.toString();
    }

    public static void readNames() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            while (true) {
                String s = reader.readLine();
                if (s.equals("end")) {
                    break;
                }
                partsNames.add(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
