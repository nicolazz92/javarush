package com.javarush.test.level18.lesson10.bonus01;

/* Шифровка
Придумать механизм шифровки/дешифровки

Программа запускается с одним из следующих наборов параметров:
-e fileName fileOutputName
-d fileName fileOutputName
где
fileName - имя файла, который необходимо зашифровать/расшифровать
fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
-e - ключ указывает, что необходимо зашифровать данные
-d - ключ указывает, что необходимо расшифровать данные
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) {
        if (args[0].equals("-e")){
            byte[] massage = readFile(args[1]);
            writeFile(encodeArray(massage), args[2]);
        } else
        if (args[0].equals("-d")){
            byte[] massage = readFile(args[1]);
            writeFile(decodeArray(massage), args[2]);
        }
    }

    public static byte[] readFile(String s) {
        try{
            FileInputStream inputStream = new FileInputStream(s);
            byte[] buffer = new byte[inputStream.available()];
            int len = inputStream.read(buffer);
            return buffer;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[]{};
    }

    public static byte[] encodeArray(byte[] original) {
        byte[] result = original;
        for (int i = 0; i < result.length; i++) {
            result[i]++;
        }
        return result;
    }

    public static byte[] decodeArray(byte[] original) {
        byte[] result = original;
        for (int i = 0; i < result.length; i++) {
            result[i]--;
        }
        return result;
    }

    public static void writeFile(byte[] massage, String way) {
        File file = new File(way);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }

            FileOutputStream outputStream = new FileOutputStream(way);
            outputStream.write(massage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
