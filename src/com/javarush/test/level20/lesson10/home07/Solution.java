package com.javarush.test.level20.lesson10.home07;

import java.io.*;

/* Переопределение сериализации в потоке
Сериализация/десериализация Solution не работает.
Исправьте ошибки не меняя сигнатуры методов и класса.
Метод main не участвует в тестировании.
Написать код проверки самостоятельно в методе main:
1) создать экземпляр класса Solution
2) записать в него данные  - writeObject
3) сериализовать класс Solution  - writeObject(ObjectOutputStream out)
4) десериализовать, получаем новый объект
5) записать в новый объект данные - writeObject
6) проверить, что в файле есть данные из п.2 и п.5
*/
public class Solution implements Serializable, AutoCloseable {
    public static void main(String[] args) throws Exception {

        Solution solOne = new Solution("/home/nicolas/dev/projects/JavaRushHomeWork/src/com/javarush/test/level20/lesson10/home07/file1");
        solOne.writeObject("One");
        solOne.close();

        File file = File.createTempFile("myTMP", null);
        FileOutputStream fileOutputStream1 = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream1);
        objectOutputStream.writeObject(solOne);
        objectOutputStream.flush();
        fileOutputStream1.close();

        Solution solTwo = new Solution("fileTwo");

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
        solTwo = (Solution) objectInputStream.readObject();
        objectInputStream.close();

        System.out.println();
    }

    public String fileName;
    private transient FileOutputStream stream;//нельзя сериализовать потоки!

    public Solution() {
    }

    public Solution(String fileName) throws FileNotFoundException {
        this.stream = new FileOutputStream(fileName);
        this.fileName = fileName;
    }

    public void writeObject(String string) throws IOException {
        stream.write(string.getBytes());
        stream.write("\n".getBytes());
        stream.flush();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
//        out.close();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.stream = new FileOutputStream(this.fileName, true);
//        in.close();
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing everything!");
        stream.close();
    }
}
