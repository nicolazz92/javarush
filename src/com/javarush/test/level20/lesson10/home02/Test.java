package com.javarush.test.level20.lesson10.home02;

import java.io.*;

/**
 * Created by nicolas on 13.05.16.
 * at 21:54
 */
public class Test {
    private static void serA() throws IOException, ClassNotFoundException {
        Solution.A a = (new Solution()).creareA();

        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(a);
        oos.flush();
        oos.close();

        FileInputStream fis = new FileInputStream("temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        Object aNew = oin.readObject();

        System.out.println();
    }

    private static void serB() throws IOException, ClassNotFoundException {
        Solution.B b = (new Solution()).creareB();

        FileOutputStream fos = new FileOutputStream("temp.out");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(b);
        oos.flush();
        oos.close();

        FileInputStream fis = new FileInputStream("temp.out");
        ObjectInputStream oin = new ObjectInputStream(fis);
        Object bNew = oin.readObject();

        System.out.println();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
//        serA();
        serB();
    }
}
