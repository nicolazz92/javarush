package com.javarush.test.level20.lesson10.home02;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

/* Десериализация
На вход подается поток, в который записан сериализованный объект класса A либо класса B.
Десериализуйте объект в методе getOriginalObject предварительно определив, какого именно типа там объект.
Реализуйте интерфейс Serializable где необходимо.
*/
public class Solution implements Serializable{
    public A getOriginalObject(ObjectInputStream objectStream) throws IOException, ClassNotFoundException {
        Object deSerObj = objectStream.readObject();
        if (deSerObj instanceof B) {
            return (B) deSerObj;
        }
        if (deSerObj instanceof A) {
            return (A) deSerObj;
        }
        return null;
    }

    public class A implements Serializable {
        static final long serialVersionUID = 1L;

        public A() {
        }
    }

    public A creareA() {
        return new A();
    }

    public class B extends A {
        static final long serialVersionUID = 2L;

        public B() {
            System.out.println("inside B");
        }
    }

    public B creareB() {
        return new B();
    }

}
