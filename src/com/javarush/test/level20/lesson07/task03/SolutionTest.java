package com.javarush.test.level20.lesson07.task03;

import java.io.*;

/**
 * Created by nicolas on 13.05.16.
 * at 2:23
 */
public class SolutionTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Solution.Person person = new Solution.Person("a", "ab", 34);
//        person.setFather(new Solution.Person());

        File mySerFile = File.createTempFile("mySerFile", null);
        ObjectOutput objectOutput = new ObjectOutputStream(new FileOutputStream(mySerFile));
        person.writeExternal(objectOutput);
        objectOutput.close();

        Solution.Person personNew = new Solution.Person();
        ObjectInput objectInput = new ObjectInputStream(new FileInputStream(mySerFile));
        personNew.readExternal(objectInput);
        objectInput.close();

        System.out.println();
    }
}
