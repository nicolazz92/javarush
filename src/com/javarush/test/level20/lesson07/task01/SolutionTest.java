package com.javarush.test.level20.lesson07.task01;

import java.io.*;

/**
 * Created by nicolas on 13.05.16.
 * at 1:33
 */
public class SolutionTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Solution.Apartment apartment = new Solution.Apartment("street", 12);
        System.out.println(apartment.toString());

        File serialFile = File.createTempFile("mySerialFile", null);

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(serialFile));
        apartment.writeExternal(objectOutputStream);
        objectOutputStream.close();

        Solution.Apartment apartmentNew = new Solution.Apartment("", 0);

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(serialFile));
        apartmentNew.readExternal(objectInputStream);
        objectInputStream.close();
        System.out.println(apartmentNew.toString());
    }
}
