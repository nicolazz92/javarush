package com.javarush.test.level20.lesson07.task02;

import java.io.*;

/**
 * Created by nicolas on 13.05.16.
 * at 1:33
 */
public class SolutionTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Solution.OutputToConsole outputToConsole = new Solution.OutputToConsole(10);

        File serialFile = File.createTempFile("mySerialFile", null);

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(serialFile));
        outputToConsole.writeExternal(objectOutputStream);
        objectOutputStream.close();

        Solution.OutputToConsole outputToConsoleNew = new Solution.OutputToConsole(10);

        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(serialFile));
        outputToConsoleNew.readExternal(objectInputStream);
        objectInputStream.close();

        System.out.println();
    }
}
