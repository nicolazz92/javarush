package com.javarush.test.level20.lesson07.task04;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/* Serializable Solution
Сериализуйте класс Solution.
Подумайте, какие поля не нужно сериализовать, пометить ненужные поля — transient.
Объект всегда должен содержать актуальные итоговые данные.
Метод main не участвует в тестировании.
Написать код проверки самостоятельно в методе main:
1) создать файл, открыть поток на чтение (input stream) и на запись(output stream)
2) создать экземпляр класса Solution - savedObject
3) записать в поток на запись savedObject (убедитесь, что они там действительно есть)
4) создать другой экземпляр класса Solution с другим параметром
5) загрузить из потока на чтение объект - loadedObject
6) проверить, что savedObject.string равна loadedObject.string
7) обработать исключения
*/
public class Solution implements Serializable{
    public static void main(String[] args){
        File mySerFile = null;
        try {
            mySerFile = File.createTempFile("mySerFile", null);
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("can't create tmp file");
        }

        Solution savedObject = new Solution(10);
        ObjectOutput objectOutput = null;
        try {
            objectOutput = new ObjectOutputStream(new FileOutputStream(mySerFile));
            objectOutput.writeObject(savedObject);
            objectOutput.flush();
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("something is wrong with serializing");
        }finally {
            try {
                assert objectOutput != null;
                objectOutput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Solution loadedObject = new Solution(0);
        ObjectInput objectInput = null;
        try {
            objectInput = new ObjectInputStream(new FileInputStream(mySerFile));
            loadedObject = (Solution) objectInput.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("something is wrong with deserializing");
        } finally {
            try {
                assert objectInput != null;
                objectInput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println(savedObject.string.equals(loadedObject.string));
    }

    private transient final String pattern = "dd MMMM yyyy, EEEE";
    private transient Date currentDate;
    private transient int temperature;
    String string;

    public Solution(int temperature) {
        this.currentDate = new Date();
        this.temperature = temperature;

        string = "Today is %s, and current temperature is %s C";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        this.string = String.format(string, format.format(currentDate), temperature);
    }

    @Override
    public String toString() {
        return this.string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Solution solution = (Solution) o;

        if (temperature != solution.temperature) return false;
        return pattern != null ? pattern.equals(solution.pattern) : solution.pattern == null;

    }

    @Override
    public int hashCode() {
        int result = pattern != null ? pattern.hashCode() : 0;
        result = 31 * result + temperature;
        return result;
    }
}
