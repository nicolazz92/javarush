package com.javarush.test.level20.lesson02.task02;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* Читаем и пишем в файл: JavaRush
Реализуйте логику записи в файл и чтения из файла для класса JavaRush
В файле your_file_name.tmp может быть несколько объектов JavaRush
Метод main реализован только для вас и не участвует в тестировании
*/
public class Solution {
    public static void main(String[] args) {
        //you can find your_file_name.tmp in your TMP directory or fix outputStream/inputStream according to your real file location
        //вы можете найти your_file_name.tmp в папке TMP или исправьте outputStream/inputStream в соответствии с путем к вашему реальному файлу
        try {
            File your_file_name = File.createTempFile("your_file_name", null);
            OutputStream outputStream = new FileOutputStream("C:\\Documents and Settings\\Николай\\dev\\projects\\JavaRushHomeWork\\src\\com\\javarush\\test\\level20\\lesson02\\task02\\file.txt");
            InputStream inputStream = new FileInputStream("C:\\Documents and Settings\\Николай\\dev\\projects\\JavaRushHomeWork\\src\\com\\javarush\\test\\level20\\lesson02\\task02\\file.txt");

            JavaRush javaRush = new JavaRush();
            //initialize users field for the javaRush object here - инициализируйте поле users для объекта javaRush тут
            initUsers(javaRush);
            javaRush.save(outputStream);
            outputStream.flush();

            JavaRush loadedObject = new JavaRush();
            loadedObject.load(inputStream);
            //check here that javaRush object equals to loadedObject object - проверьте тут, что javaRush и loadedObject равны
            System.out.println(javaRush.equals(loadedObject));

            outputStream.close();
            inputStream.close();

        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with my file");
        } catch (Exception e) {
            //e.printStackTrace();
            System.out.println("Oops, something wrong with save/load method");
        }
    }

    private static void initUsers(JavaRush javaRush) {
        User firstUser = new User();
        firstUser.setFirstName("Nestor");
        firstUser.setLastName("Nefedov");
        firstUser.setBirthDate(new Date(1312312312321312312L));
        firstUser.setCountry(User.Country.RUSSIA);
        javaRush.users.add(firstUser);

        User secondUser = new User();
        secondUser.setFirstName("Alex");
        secondUser.setBirthDate(new Date(323232322323232323L));
        javaRush.users.add(secondUser);
    }

    public static class JavaRush {
        public List<User> users = new ArrayList<>();

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            JavaRush javaRush = (JavaRush) o;

            return users != null ? users.equals(javaRush.users) : javaRush.users == null;

        }

        @Override
        public int hashCode() {
            return users != null ? users.hashCode() : 0;
        }

        public void save(OutputStream outputStream) throws Exception {
            PrintWriter writer = new PrintWriter(outputStream);

            for (User user : users) {
                writer.println("@@");//new user
                if (user.getFirstName() != null) {
                    writer.println("firstName::" + user.getFirstName());
                }
                if (user.getLastName() != null) {
                    writer.println("lastName::" + user.getLastName());
                }
                if (user.getBirthDate() != null) {
                    writer.println("birthDate::" + user.getBirthDate().getTime());
                }
                writer.println("male::" + user.isMale());
                if (user.getCountry() != null) {
                    writer.println("country::" + user.getCountry().getDisplayedName());
                }
                writer.println("@@__@@");
            }

            writer.close();
        }

        public void load(InputStream inputStream) throws Exception {
            //implement this method - реализуйте этот метод
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

            while (true) {
                String curString = reader.readLine();
                if (curString == null || curString.equals("")) {
                    break;
                }
                User loadedUser = userLoader(reader, curString);
                users.add(loadedUser);
            }

            reader.close();
        }

        private User userLoader(BufferedReader reader, String firstStr) throws IOException {
            String curStr = firstStr;
            User curUser = new User();
            while (!curStr.equals("@@__@@")) {
                if (curStr.equals("@@")) {
                    curStr = reader.readLine();
                    continue;
                }
                String[] value = curStr.split("::");
                String arg = value[0];
                String val = value[1];
                if (arg.equals("firstName")) {
                    curUser.setFirstName(val);
                    curStr = reader.readLine();
                    continue;
                }
                if (arg.equals("lastName")) {
                    curUser.setLastName(val);
                    curStr = reader.readLine();
                    continue;
                }
                if (arg.equals("birthDate")) {
                    curUser.setBirthDate(new Date(Long.parseLong(val)));
                    curStr = reader.readLine();
                    continue;
                }
                if (arg.equals("male")) {
                    curUser.setMale(Boolean.parseBoolean(val));
                    curStr = reader.readLine();
                    continue;
                }
                if (arg.equals("country")) {
                    if (val.equals("Russia")) {
                        curUser.setCountry(User.Country.RUSSIA);
                        curStr = reader.readLine();
                        continue;
                    }
                    if (val.equals("Ukraine")) {
                        curUser.setCountry(User.Country.UKRAINE);
                        curStr = reader.readLine();
                        continue;
                    }
                    if (val.equals("Other")) {
                        curUser.setCountry(User.Country.OTHER);
                        curStr = reader.readLine();
//                        continue;
                    }
                }
            }

            return curUser;
        }
    }
}
