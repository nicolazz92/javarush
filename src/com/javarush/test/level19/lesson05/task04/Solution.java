package com.javarush.test.level19.lesson05.task04;

/* Замена знаков
Считать с консоли 2 имени файла.
Первый Файл содержит текст.
Заменить все точки "." на знак "!", вывести во второй файл.
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        FileReader firstFile = new FileReader(bufferedReader.readLine());
        FileWriter secondFile = new FileWriter(bufferedReader.readLine());
        bufferedReader.close();

        String fileString = readFile(firstFile);
        firstFile.close();

        fileString = fileString.replace(String.valueOf("."), "!");

        secondFile.append(fileString);
        secondFile.flush();
        secondFile.close();
    }

    private static String readFile(FileReader file) throws IOException {
        String fileString = String.valueOf("");
        while (file.ready()) {
            fileString += (char) file.read();
        }
        return fileString;
    }

}
