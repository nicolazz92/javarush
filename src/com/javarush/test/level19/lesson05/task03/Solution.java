package com.javarush.test.level19.lesson05.task03;

/* Выделяем числа
Считать с консоли 2 имени файла.
Вывести во второй файл все числа, которые есть в первом файле.
Числа выводить через пробел.
Закрыть потоки. Не использовать try-with-resources

Пример тела файла:
12 text var2 14 8v 1

Результат:
12 14 1
*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        FileReader firstFile = new FileReader(bufferedReader.readLine());
        FileWriter secondFile = new FileWriter(bufferedReader.readLine());
        bufferedReader.close();

        String[] words = getWords(firstFile);

        writeNumbers(secondFile, words);

        firstFile.close();
        secondFile.close();

        System.out.println();
    }

    private static void writeNumbers(FileWriter file, String[] words) throws IOException {
        String numbers = "0123456789-.e";

        for (String word : words) {
            boolean isNum = word.length() > 0;
            for (int i = 0; i < word.length(); i++) {
                isNum = isNum && numbers.indexOf(word.charAt(i)) > -1;
            }
            if (isNum){
                file.append(word).append(" ");
            }
        }
    }

    private static String[] getWords(FileReader file) throws IOException {
        String fileString = String.valueOf("");
        while (file.ready()) {
            fileString += (char) file.read();
        }
        String[] words = fileString.split(" ");

        for (int i = 0; i < words.length; i++) {
            if (words[i].contains("\n")) {
                words[i] = new StringBuilder(words[i]).delete(words[i].indexOf("\n"), words[i].indexOf("\n") + 2).toString();
            }
        }

        return words;
    }
}
