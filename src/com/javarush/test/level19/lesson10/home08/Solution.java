package com.javarush.test.level19.lesson10.home08;

/* Перевертыши
1 Считать с консоли имя файла.
2 Для каждой строки в файле:
2.1 переставить все символы в обратном порядке
2.2 вывести на экран
3 Закрыть потоки. Не использовать try-with-resources

Пример тела входного файла:
я - программист.
Амиго

Пример результата:
.тсиммаргорп - я
огимА
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        String fileName = readName();

        BufferedReader fileReader = new BufferedReader(new FileReader(fileName));
        while (true){
            String str = fileReader.readLine();
            if (str == null){
                break;
            }
            System.out.printf("%s\n", reverse(str));
        }
        fileReader.close();
    }

    private static String reverse(String str) {
        StringBuilder string = new StringBuilder(str);
        string.reverse();
        return string.toString();
    }

    private static String readName() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String result = reader.readLine();
        reader.close();
        return result;
    }
}
