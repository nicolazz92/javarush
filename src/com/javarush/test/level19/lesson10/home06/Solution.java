package com.javarush.test.level19.lesson10.home06;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/* Замена чисел
1. В статическом блоке инициализировать словарь map парами [число-слово] от 0 до 12 включительно
Например, 0 - "ноль", 1 - "один", 2 - "два"
2. Считать с консоли имя файла
3. Заменить все числа на слова используя словарь map
4. Результат вывести на экран
5. Закрыть потоки. Не использовать try-with-resources

Пример данных:
Это стоит 1 бакс, а вот это - 12 .
Переменная имеет имя file1.
110 - это число.

Пример вывода:
Это стоит один бакс, а вот это - двенадцать .
Переменная имеет имя file1.
110 - это число.
*/

public class Solution {
    public static Map<Integer, String> map = new HashMap<>();

    static {
        map.put(12,"двенадцать");
        map.put(11,"одинадцать");
        map.put(10,"десять");
        map.put(9,"девять");
        map.put(8,"восемь");
        map.put(7,"семь");
        map.put(6,"шесть");
        map.put(5,"пять");
        map.put(4,"четыре");
        map.put(3,"три");
        map.put(2,"два");
        map.put(1,"один");
        map.put(0,"ноль");
    }

    public static void main(String[] args) throws IOException {
        String fileName = getFileName();
        String origText = getOrigText(fileName);
        String[] splitedText = origText.split(" ");
        splitedText = replaceDigits(splitedText);
        print(splitedText);
    }

    private static void print(String[] splitedText) {
        for (String aSplitedText : splitedText) {
            System.out.printf("%s ", aSplitedText);
        }
    }

    private static String[] replaceDigits(String[] splitedText) {
        for (int i = 0; i < splitedText.length; i++) {
            if (isDigit(splitedText[i])){
                splitedText[i] = replaceByWord(splitedText[i]);
            }
        }
        return splitedText;
    }

    private static String replaceByWord(String s) {
        int res = -1;
        try {
            res = Integer.parseInt(s);
        } catch (NumberFormatException e){
            e.printStackTrace();
        }
        if (res >=0 && res <=12){
            return map.get(res);
        } else {
            return s;
        }
    }

    private static boolean isDigit(String s) {
        return !s.equals("") &&
                Pattern.compile("\\d").matcher(s).find() &&
                !Pattern.compile("\\D").matcher(s).find();
    }

    private static String getOrigText(String fileName) throws IOException {
        String result = String.valueOf("");
        Reader reader = new FileReader(fileName);
        while (reader.ready()) {
            result += (char) reader.read();
        }
        reader.close();
        return result;
    }

    private static String getFileName() throws IOException {
        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
        String result = consoleReader.readLine();
        consoleReader.close();
        return result;
    }
}
