package com.javarush.test.level19.lesson10.home03;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/* Хуан Хуанович
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя день месяц год
где [имя] - может состоять из нескольких слов, разделенных пробелами, и имеет тип String
[день] - int, [месяц] - int, [год] - int
данные разделены пробелами

Заполнить список PEOPLE импользуя данные из файла
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Иванов Иван Иванович 31 12 1987
Вася 15 5 2013
*/

public class Solution {
    public static final List<Person> PEOPLE = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));

        String basicString;
        while (true) {
            basicString = reader.readLine();
            if (basicString == null || Objects.equals(basicString, "")) {
                break;
            }
            String[] splitedBasicString = basicString.split(" ");

            String name = String.valueOf("");
            for (String aSplitedBasicString : splitedBasicString) {
                if (Pattern.compile("\\D").matcher(aSplitedBasicString).find()) {
                    name += aSplitedBasicString + " ";
                } else {
                    break;
                }
            }
            int day = Integer.parseInt(splitedBasicString[splitedBasicString.length - 3]);
            int month = Integer.parseInt(splitedBasicString[splitedBasicString.length - 2]) - 1;
            int year = Integer.parseInt(splitedBasicString[splitedBasicString.length - 1]);

            PEOPLE.add(new Person(name.trim(), new Date(year, month, day)));

            System.out.printf("");
        }
        reader.close();
        System.out.printf("");
    }

}
