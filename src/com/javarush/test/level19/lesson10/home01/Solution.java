package com.javarush.test.level19.lesson10.home01;

/* Считаем зарплаты
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Все данные вывести в консоль, предварительно отсортировав в возрастающем порядке по имени
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 2
Сидоров 6
Иванов 1.35
Петров 3.1

Пример вывода:
Иванов 1.35
Петров 5.1
Сидоров 6.0
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(args[0]));

        Map<String, Double> map = new TreeMap<>();
        String basicString;
        while (true) {
            basicString = reader.readLine();
            if (basicString == null || Objects.equals(basicString, "")){
                break;
            }
            String[] splitedBasicString = basicString.split(" ");
            if (!map.containsKey(splitedBasicString[0])) {
                map.put(splitedBasicString[0], Double.parseDouble(splitedBasicString[1]));
            } else {
                map.put(splitedBasicString[0], Double.parseDouble(splitedBasicString[1]) + map.get(splitedBasicString[0]));
            }
        }
        reader.close();

        Set<Map.Entry<String, Double>> unsortedSet = map.entrySet();

        Set<Map.Entry<String, Double>> treeSet = new TreeSet<>(new Comparator<Map.Entry<String, Double>>() {
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });

        for (Map.Entry<String, Double> m : unsortedSet) {
            treeSet.add(m);
        }

        for (Map.Entry<String, Double> entry : treeSet) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
    }
}
