package com.javarush.test.level19.lesson10.home05;

/* Слова с цифрами
В метод main первым параметром приходит имя файла1, вторым - файла2.
Файл1 содержит строки со слов, разделенные пробелом.
Записать через пробел в Файл2 все слова, которые содержат цифры, например, а1 или abc3d
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.*;
import java.util.regex.Pattern;

public class Solution {
    public static void main(String[] args) throws IOException {
        String firstFile = args[0];
        String secondFile = args[1];

        String[] origWords = getOrigWords(firstFile);

        writeWords(secondFile, origWords);
    }

    private static void writeWords(String secondFile, String[] origWords) throws IOException {
        Writer secondFileWriter = new FileWriter(secondFile);

        for (String word : origWords) {
            if (wordHasDisticts(word)){
                secondFileWriter.append(word);
                secondFileWriter.append(" ");
                secondFileWriter.flush();
            }
        }
        secondFileWriter.close();
    }

    private static boolean wordHasDisticts(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (Pattern.compile("\\d").matcher(String.valueOf(word.charAt(i))).find()){
                return true;
            }
        }
        return false;
    }

    private static String[] getOrigWords(String firstFile) throws IOException {
        Reader firstFileReader = new FileReader(firstFile);
        String origWords = String.valueOf("");
        while (firstFileReader.ready()){
            origWords += (char) firstFileReader.read();
        }
        firstFileReader.close();
        return String.valueOf(origWords).split(" ");
    }
}
