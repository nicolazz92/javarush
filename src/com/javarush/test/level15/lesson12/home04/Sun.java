package com.javarush.test.level15.lesson12.home04;

/**
 * Created by nicolas on 09.04.16.
 * at 12:51
 */
public class Sun implements Planet{

    private static Sun instance;

    private Sun(){}

    public static Sun getInstance(){
        if(instance == null){
            instance = new Sun();
        }
        return instance;
    }
}