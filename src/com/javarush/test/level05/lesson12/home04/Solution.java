package com.javarush.test.level05.lesson12.home04;

/* Вывести на экран сегодняшнюю дату
Вывести на экран текущую дату в аналогичном виде "21 02 2014".
*/

import java.util.Arrays;
import java.util.Date;

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
        Date d = new Date();
        String[] date = String.valueOf(d).split(" ");
        String[] out = new String[3];
        out[0] = date[2];
        out[1] = String.valueOf(d.getMonth() + 1);
        out[2] = date[5];

        System.out.println(out[0] + " 0" + out[1] + " " + out[2]);
    }
}
