package com.javarush.test.level05.lesson12.home03;

/* Создай классы Dog, Cat, Mouse
Создай классы Dog, Cat, Mouse. Добавь по три поля в каждый класс, на твой выбор.
Создай объекты для героев мультика Том и Джерри.
Так много, как только вспомнишь.
Пример:
Mouse jerryMouse = new Mouse(“Jerry”, 12 , 5), где 12 - высота в см, 5 - длина хвоста в см.
*/

public class Solution {
    public static void main(String[] args) {
        Mouse jerryMouse = new Mouse("Jerry", 12, 5);
        Mouse pekusMouse = new Mouse("uncle Pekus", 11, 6);
        Cat tomCat = new Cat("Tom", 4, 4);
        Cat bitchCat = new Cat("Bitch", 3, 2);
        Dog dog1 = new Dog("Dog", 11, "black");


        //напишите тут ваш код
    }

    public static class Mouse {
        String name;
        int height;
        int tail;

        public Mouse(String name, int height, int tail) {
            this.name = name;
            this.height = height;
            this.tail = tail;
        }
    }

    public static class Cat {
        String name;
        int age;
        int speed;

        public Cat(String name, int age, int speed) {
            this.name = name;
            this.age = age;
            this.speed = speed;
        }
    }

    public static class Dog {
        String name;
        int force;
        String color;

        public Dog(String name, int force, String color) {
            this.name = name;
            this.force = force;
            this.color = color;
        }
    }
}
