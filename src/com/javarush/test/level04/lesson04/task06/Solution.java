package com.javarush.test.level04.lesson04.task06;

/* День недели
Ввести с клавиатуры номер дня недели, в зависимости от номера вывести название «понедельник», «вторник», «среда»,
«четверг», «пятница», «суббота», «воскресенье»,
если введен номер больше или меньше 7 – вывести «такого дня недели не существует».
Пример для номера 5:
пятница
Пример для номера 10:
такого дня недели не существует
*/

import java.io.*;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        write();
    }

    public static void write(){
        int day = read();
        String[] week = new String[]{"понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"};
        try{
            System.out.println(week[day - 1]);
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("такого дня недели не существует");
        }
    }

    public static int read()
    {
        try{
            InputStreamReader inputStreamReader = new InputStreamReader(System.in);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            return Integer.parseInt(bufferedReader.readLine());
        } catch (IOException ignored){

        }
        return 0;
    }

}