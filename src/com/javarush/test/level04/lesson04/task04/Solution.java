package com.javarush.test.level04.lesson04.task04;

/* Время года
Реализовать метод checkSeason. По номеру месяца, метод должен определить время года (зима, весна, лето, осень) и вывести на экран.
Пример для номера месяца 2:
зима
Пример для номера месяца 5:
весна
*/

public class Solution
{
    public static void main(String[] args) {
        checkSeason(12);
        checkSeason(4);
        checkSeason(7);
        checkSeason(10);
    }

    public static void checkSeason(int mount){
        //::CODE:
        mount--;
        switch (mount) {
            case 0:
            case 1:
            case 11:
                System.out.println("зима");
                break;
            case 2:
            case 3:
            case 4:
                System.out.println("весна");
                break;
            case 5:
            case 6:
            case 7:
                System.out.println("лето");
                break;
            case 8:
            case 9:
            case 10:
                System.out.println("осень");
                break;
        }
    }
}