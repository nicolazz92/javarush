package com.javarush.test.level08.lesson08.task05;

import java.util.*;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        //напишите тут ваш код
        HashMap<String, String> map = new HashMap<>();

        map.put("Иванов1", "Иван");
        map.put("Иванов2", "Сидор");
        map.put("Иванов3", "Пётр");
        map.put("Сидоров1", "Иван");
        map.put("Сидоров2", "Сидор");
        map.put("Сидоров3", "Пётр");
        map.put("Петров1", "Иван");
        map.put("Петров2", "Сидор");
        map.put("Петров3", "Пётр");
        map.put("Петров4", "Максим");

        return map;
    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map) {
        //напишите тут ваш код
        Map<String, String> mapCopy = new HashMap<>(map);
        for (Map.Entry<String, String> s : mapCopy.entrySet()) {
            int count = 0;
            for (Map.Entry<String, String> str : map.entrySet()) {
                if (s.getValue().equals(str.getValue())) {
                    count++;
                }
            }
            if (count > 1) {
                removeItemFromMapByValue(map, s.getValue());
            }
            count = 0;
        }
    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value) {
        HashMap<String, String> copy = new HashMap<>(map);
        for (Map.Entry<String, String> pair : copy.entrySet()) {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

    public static void main(String[] args) {
        HashMap<String, String> map = createMap();
        removeTheFirstNameDuplicates(map);
        for (Map.Entry<String, String> m : map.entrySet()) {
            System.out.println(m);
        }
    }
}
