package com.javarush.test.level08.lesson08.task04;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* Удалить всех людей, родившихся летом
Создать словарь (Map<String, Date>) и занести в него десять записей по принципу: «фамилия» - «дата рождения».
Удалить из словаря всех людей, родившихся летом.
*/

public class Solution {
    public static HashMap<String, Date> createMap() {
        HashMap<String, Date> map = new HashMap<>();
        map.put("Stallone", new Date("JUNE 1 1980"));
        map.put("A", new Date("January 1 1980"));
        map.put("B", new Date("February 1 1980"));
        map.put("C", new Date("March 1 1980"));
        map.put("D", new Date("April 1 1980"));
        map.put("E", new Date("May 1 1980"));
        map.put("J", new Date("June 1 1980"));
        map.put("S", new Date("July 1 1980"));
        map.put("T", new Date("August 1 1980"));
        map.put("N", new Date("September 1 1980"));

        //напишите тут ваш код
        return map;
    }

    public static void removeAllSummerPeople(HashMap<String, Date> map) {
        //напишите тут ваш код
        Map<String, Date> buf = new HashMap<>(map);
        for(Map.Entry<String, Date> pair : buf.entrySet()) {
            if (pair.getValue().getMonth() >= 5 && pair.getValue().getMonth() <= 7){
                map.remove(pair.getKey());
            }
        }
    }

    public static void main(String[] args) {
        HashMap<String, Date> a = createMap();
        removeAllSummerPeople(a);
        for (Map.Entry<String, Date> m: a.entrySet()) {
            System.out.println(m);
        }
    }
}