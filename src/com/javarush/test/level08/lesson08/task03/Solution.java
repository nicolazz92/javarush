package com.javarush.test.level08.lesson08.task03;

import java.util.*;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution {
    public static HashMap<String, String> createMap() {
        //напишите тут ваш код
        Map<String, String> map = new HashMap<>();
        map.put("Иванов1", "Иван");
        map.put("Иванов2", "Сидор");
        map.put("Иванов3", "Пётр");
        map.put("Сидоров1", "Иван");
        map.put("Сидоров2", "Сидор");
        map.put("Сидоров3", "Пётр");
        map.put("Петров1", "Иван");
        map.put("Петров2", "Сидор");
        map.put("Петров3", "Пётр");
        map.put("Петров4", "Максим");

        return (HashMap<String, String>) map;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name) {
        //напишите тут ваш код
        int count = 0;
        for (Map.Entry<String, String> m : map.entrySet()) {
            if (m.getValue().equals(name)){
                count++;
            }
        }
        return count;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName) {
        //напишите тут ваш код
        int count = 0;
        for (Map.Entry<String, String> m : map.entrySet()) {
            if (m.getKey().equals(lastName)){
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println(getCountTheSameFirstName(createMap(), "Максим"));
        System.out.println(getCountTheSameFirstName(createMap(), "Сидор"));

        System.out.println(getCountTheSameLastName(createMap(), "Иванов1"));
        System.out.println(getCountTheSameLastName(createMap(), "Петров1"));
    }
}
