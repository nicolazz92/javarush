package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution {
    public static HashSet<Integer> createSet() {
        //напишите тут ваш код
        Set<Integer> set = new HashSet<>();

        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            set.add(random.nextInt());
        }

        return (HashSet<Integer>) set;
    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set) {
        //напишите тут ваш код
        Integer[] arr = set.toArray(new Integer[set.size()]);
        set.clear();
        for (Integer anArr : arr) {
            if (anArr <= 10)
                set.add(anArr);
        }

        return set;
    }

    public static void main(String[] args) {
        System.out.println(removeAllNumbersMoreThan10(createSet()).toString());
    }
}
