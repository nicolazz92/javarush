package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        //напишите тут ваш код
        Human ch1 = new Human("ch1", true, 11);
        Human ch2 = new Human("ch2", true, 10);
        Human ch3 = new Human("ch3", true, 9);

        ArrayList<Human> buf = new ArrayList<>();
        buf.add(ch1);
        buf.add(ch2);
        buf.add(ch3);
        Human mom = new Human("mom", false, 30, buf);
        Human father = new Human("father", true, 31, buf);

        buf.clear();
        buf.add(mom);
        Human grandF1 = new Human("GF1", true, 78, buf);
        Human grandM1 = new Human("GM1", false, 76, buf);

        buf.clear();
        buf.add(father);
        Human grandF2 = new Human("GF2", true, 78, buf);
        Human grandM2 = new Human("GM2", false, 76, buf);

        System.out.println(grandF1);
        System.out.println(grandM1);
        System.out.println(grandF2);
        System.out.println(grandM2);
        System.out.println(mom);
        System.out.println(father);
        System.out.println(ch1);
        System.out.println(ch2);
        System.out.println(ch3);
    }

    public static class Human {
        //напишите тут ваш код
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children = new ArrayList<>();

        public Human(String name, boolean sex, int age, ArrayList<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = new ArrayList<>(children);
        }

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public String toString() {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0) {
                text += ", дети: " + this.children.get(0).name;

                for (int i = 1; i < childCount; i++) {
                    Human child = this.children.get(i);
                    text += ", " + child.name;
                }
            }

            return text;
        }
    }

}
