package com.javarush.test.level08.lesson11.home05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Мама Мыла Раму. Теперь с большой буквы
Написать программу, которая вводит с клавиатуры строку текста.
Программа заменяет в тексте первые буквы всех слов на заглавные.
Вывести результат на экран.

Пример ввода:
  мама     мыла раму.

Пример вывода:
  Мама     Мыла Раму.
*/

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s = reader.readLine();

        StringBuilder stringBuilder = new StringBuilder(s);
        if (stringBuilder.charAt(0) != ' ') {
            stringBuilder.setCharAt(0, Character.toUpperCase(stringBuilder.charAt(0)));
        }
        for (int i = 0; i < stringBuilder.length(); i++) {
            if (stringBuilder.charAt(i) == ' ' && i < stringBuilder.length() - 1 && stringBuilder.charAt(i + 1) != ' ') {
                stringBuilder.setCharAt(i + 1, Character.toUpperCase(stringBuilder.charAt(i + 1)));
            }
        }

        System.out.println(stringBuilder.toString());
        //напишите тут ваш код
    }


}
