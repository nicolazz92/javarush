package com.javarush.test.level03.lesson06.task01;

/* Мама мыла раму
Вывести на экран все возможные комбинации слов «Мама», «Мыла», «Раму».
Подсказка: их 6 штук. Каждую комбинацию вывести с новой строки. Слова не разделять. Пример:
МылаРамуМама
РамуМамаМыла
...
*/

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
        String[] arr = new String[]{"Мама", "Мыла", "Раму"};
        System.out.println(arr[0] + arr[1] + arr[2]);
        System.out.println(arr[0] + arr[2] + arr[1]);
        System.out.println(arr[1] + arr[0] + arr[2]);
        System.out.println(arr[1] + arr[2] + arr[0]);
        System.out.println(arr[2] + arr[1] + arr[0]);
        System.out.println(arr[2] + arr[0] + arr[1]);
    }
}