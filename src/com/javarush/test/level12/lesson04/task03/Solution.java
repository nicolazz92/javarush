package com.javarush.test.level12.lesson04.task03;

/* Пять методов print с разными параметрами
Написать пять методов print с разными параметрами.
*/

import java.util.Date;

public class Solution {
    public static void main(String[] args) {

    }

    public static void print(int n) {
        System.out.println(n);
    }

    public static void print(Integer n) {
        System.out.println(n);
    }

    public static void print(String n) {
        System.out.println(n);
    }

    public static void print(Date n) {
        System.out.println(n);
    }

    public static void print(Float n) {
        System.out.println(n);
    }

}
