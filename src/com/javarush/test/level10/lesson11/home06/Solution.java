package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution {
    public static void main(String[] args) {

    }

    public static class Human {
        String name;
        boolean sex;
        int age;
        boolean prettyFace;
        long money;
        short dickLength;

        public Human(String name, boolean sex, int age, boolean prettyFace, long money, short dickLength) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.prettyFace = prettyFace;
            this.money = money;
            this.dickLength = dickLength;
        }

        public Human(String name, boolean sex, int age, boolean prettyFace, short dickLength, long money) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.prettyFace = prettyFace;
            this.dickLength = dickLength;
            this.money = money;
        }

        public Human(String name, boolean sex, int age, long money, boolean prettyFace, short dickLength) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.money = money;
            this.prettyFace = prettyFace;
            this.dickLength = dickLength;
        }

        public Human(String name, boolean sex, boolean prettyFace, int age, long money, short dickLength) {
            this.name = name;
            this.sex = sex;
            this.prettyFace = prettyFace;
            this.age = age;
            this.money = money;
            this.dickLength = dickLength;
        }

        public Human(String name, int age, boolean sex, boolean prettyFace, long money, short dickLength) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.prettyFace = prettyFace;
            this.money = money;
            this.dickLength = dickLength;
        }

        public Human(boolean sex, String name, int age, boolean prettyFace, long money, short dickLength) {
            this.sex = sex;
            this.name = name;
            this.age = age;
            this.prettyFace = prettyFace;
            this.money = money;
            this.dickLength = dickLength;
        }

        public Human(String name, boolean sex, int age, boolean prettyFace, long money) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.prettyFace = prettyFace;
            this.money = money;
        }

        public Human(String name, boolean sex, int age, boolean prettyFace) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.prettyFace = prettyFace;
        }

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex) {
            this.name = name;
            this.sex = sex;
        }
    }

}
