package com.javarush.test.level06.lesson08.task05;

/* Класс StringHelper
Cделать класс StringHelper, у которого будут 2 статических метода:
String multiply(String s, int count) – возвращает строку повторенную count раз.
String multiply(String s) – возвращает строку повторенную 5 раз.
Пример:
Амиго -> АмигоАмигоАмигоАмигоАмиго
*/

public class StringHelper {
    public static String multiply(String s) {
        //напишите тут ваш код
        StringBuilder stringBuilder = new StringBuilder("");
        for (int i = 0; i < 5; i++) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString();
    }

    public static String multiply(String s, int count) {
        //напишите тут ваш код
        StringBuilder stringBuilder = new StringBuilder("");
        for (int i = 0; i < count; i++) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString();
    }
}
