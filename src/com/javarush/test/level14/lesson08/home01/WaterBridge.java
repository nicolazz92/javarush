package com.javarush.test.level14.lesson08.home01;

/**
 * Created by nicolas on 04.04.16.
 * at 19:03
 */
public class WaterBridge implements Bridge {
    @Override
    public int getCarsCount() {
        return 7;
    }
}
