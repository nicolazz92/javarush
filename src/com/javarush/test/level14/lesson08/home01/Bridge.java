package com.javarush.test.level14.lesson08.home01;

/**
 * Created by nicolas on 04.04.16.
 * at 19:02
 */
public interface Bridge {
    int getCarsCount();
}
