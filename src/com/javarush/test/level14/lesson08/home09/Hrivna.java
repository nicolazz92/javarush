package com.javarush.test.level14.lesson08.home09;

/**
 * Created by nicolas on 06.04.16.
 * at 13:36
 */
public class Hrivna extends Money {
    public Hrivna(double amount) {
        super(amount);
    }

    @Override
    public String getCurrencyName() {
        return "HRN";
    }
}
