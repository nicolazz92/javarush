package com.javarush.test.level14.lesson08.home09;

public abstract class Money {
    private double m;

    public Money(double amount) {
        m = amount;
    }

    public double getAmount() {
        return m;
    }

    public abstract String getCurrencyName();
}

