package com.javarush.test.level14.lesson08.home05;

/**
 * Created by nicolas on 04.04.16.
 * at 19:33
 */
public class Mouse implements CompItem {
    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
}
