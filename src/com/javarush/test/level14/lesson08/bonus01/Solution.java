package com.javarush.test.level14.lesson08.bonus01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

/* Нашествие эксепшенов
Заполни массив exceptions 10 различными эксепшенами.
Первое исключение уже реализовано в методе initExceptions.
*/

public class Solution {
    public static List<Exception> exceptions = new ArrayList<>();

    public static void main(String[] args) {
        initExceptions();

        for (Exception exception : exceptions) {
            System.out.println(exception);
        }
    }

    private static void initExceptions() {   //it's first exception
        try {
            float i = 1 / 0;
        } catch (Exception e) {
            exceptions.add(e);//Arithmetic
        }

        //Add your code here
        try {
            Object x = new Integer(0);
            System.out.println((String) x);
        } catch (Exception e) {
            exceptions.add(e);//CastClass
        }

        try {
            Object x[] = new String[3];
            x[0] = new Integer(0);
        } catch (Exception e) {
            exceptions.add(e);//ArrayStoreException
        }

        try {
            Stack t = new Stack();
            System.out.println(t.pop());
        } catch (Exception e) {
            exceptions.add(e);//EmptyStackException
        }

        try {
            Integer t = null;
            System.out.println(t * 2);
        } catch (Exception e) {
            exceptions.add(e);//NullPointer
        }

        try {
            Stack t = new Stack();
            System.out.println(t.get(2));
        } catch (Exception e) {
            exceptions.add(e);//ArrayIndexBoundOf
        }

        try {
            LinkedList t = new LinkedList();
            System.out.println(t.get(2));
        } catch (Exception e) {
            exceptions.add(e);//IndexBoundOf
        }

        try {
            List t = new ArrayList();
            Iterator i = t.iterator();
            while (true) {
                i.next();
            }
        } catch (Exception e) {
            exceptions.add(e);//NoSuchElem
        }

        try {
            Set set = Collections.unmodifiableSet(new HashSet());
            set.add(2);
        } catch (Exception e) {
            exceptions.add(e);//UnsupportedOperationException
        }

        try {
            FileInputStream fl = new FileInputStream("C:\\TEST.txt");
            System.out.println(fl.toString());
        } catch (FileNotFoundException e) {
            exceptions.add(e);//FileNotFoundException
        }

    }
}
