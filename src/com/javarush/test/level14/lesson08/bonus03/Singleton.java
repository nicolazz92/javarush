package com.javarush.test.level14.lesson08.bonus03;

/**
 * Created by nicolas on 06.04.16.
 * at 16:45
 */
public class Singleton {
    private Singleton() {
    }

    private static Singleton instance;

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
