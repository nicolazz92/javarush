package com.javarush.test.level14.lesson06.home01;

/**
 * Created by Николай on 04.04.16.
 */
public abstract class Hen {
    abstract int getCountOfEggsPerMonth();

    String getDescription() {
        return "Я курица.";
    }
}

